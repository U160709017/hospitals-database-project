load data infile '/var/lib/mysql-files/hospital_types_NEW.csv'
into table Hospitals.hospital_types fields terminated by ','
lines terminated by '\r' (hospital_types.hospital_type);

select * from hospital_types;
#--------------------------------------------------------------------------------
load data infile '/var/lib/mysql-files/trauma_types_NEW.csv'
into table Hospitals.trauma_types fields terminated by ','
lines terminated by '\r' (trauma_types.trauma_type, trauma_types.trauma_level);

select * from trauma_types;
#--------------------------------------------------------------------------------
load data infile '/var/lib/mysql-files/hospitalsNEW.csv'
into table Hospitals.hospitals fields terminated by ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n' (hospital_id,hospital_name,hospital_telephone,hospital_status,hospital_owner,hospital_website,hospital_type_id);

select * from hospitals;
#--------------------------------------------------------------------------------
load data infile '/var/lib/mysql-files/trauma_types_NEW.csv'
into table Hospitals.trauma_types fields terminated by ','
lines terminated by '\n' (trauma_types.trauma_type, trauma_types.trauma_level);

select * from trauma_types;
#--------------------------------------------------------------------------------
load data infile '/var/lib/mysql-files/countries.csv'
into table Hospitals.countries fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\r\n' (countries.country_name);

select * from countries;
