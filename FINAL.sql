ALTER TABLE hospital_types AUTO_INCREMENT = 46;

load data infile '/var/lib/mysql-files/hospital_types_NEW.csv'
into table Hospitals.hospital_types fields terminated by ','
lines terminated by '\r\n' (hospital_types.hospital_type);

delete from hospital_types;
select * from hospital_types;
#--------------------------------------------------------------------------------
ALTER TABLE trauma_types AUTO_INCREMENT = 94;

load data infile '/var/lib/mysql-files/trauma_types_NEW.csv'
into table Hospitals.trauma_types fields terminated by ','
lines terminated by '\n' (trauma_types.trauma_type, trauma_types.trauma_level);

select * from trauma_types;
#--------------------------------------------------------------------------------
load data infile '/var/lib/mysql-files/hospitalsNEW.csv'
into table Hospitals.hospitals fields terminated by ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n' (hospital_id,hospital_name,hospital_telephone,hospital_status,hospital_owner,hospital_website,hospital_type_id,hospitals.trauma_type_id,alternate_name);

select * from hospitals;
delete from hospitals;
alter table hospitals add column alternate_name VARCHAR(150);
#--------------------------------------------------------------------------------
ALTER TABLE countries AUTO_INCREMENT = 23;

load data infile '/var/lib/mysql-files/countries.csv'
into table Hospitals.countries fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\r\n' (countries.country_name);

delete from countries;
select * from countries;
#--------------------------------------------------------------------------------
ALTER TABLE states AUTO_INCREMENT = 41145;

delete from states;

load data infile '/var/lib/mysql-files/states.csv'
into table Hospitals.states fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\n' (state_name, state_FIBS, country_id);

select * from states;
#--------------------------------------------------------------------------------
ALTER TABLE counties AUTO_INCREMENT = 50;

load data infile '/var/lib/mysql-files/counties.csv'
into table Hospitals.counties fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\n' (county_name, county_FIBS, state_id);

delete from counties;
select * from counties;
#--------------------------------------------------------------------------------
alter table cities add column county_name VARCHAR(45);
alter table cities add column county_FIBS INT;

load data infile '/var/lib/mysql-files/cities.csv'
into table Hospitals.cities fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\n' (city_name, county_name,county_FIBS);

select * from cities;
select distinct city_name from cities;
delete from cities;

select distinct city_name, county_name, county_FIBS
from cities;
select distinct *
from cities;

load data infile '/var/lib/mysql-files/citiestempX11.csv'
into table Hospitals.cities fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\n' (city_name, county_name, county_FIBS, county_id);

select distinct city_name, county_name, county_FIBS, county_id
from cities;

load data infile '/var/lib/mysql-files/citiestemp.csv'
into table Hospitals.cities fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\n' (city_name, county_name, county_FIBS);

select t.city_name, c.county_name, c.county_FIBS, c.county_id
from counties as c join cities as t on c.county_name = t.county_name and c.county_FIBS = t.county_FIBS;

load data infile '/var/lib/mysql-files/citiestemp.csv'
into table Hospitals.cities fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\n' (city_name, county_id);

alter table cities drop county_name;
alter table cities drop county_FIBS;
#--------------------------------------------------------------------------------
select * from addresses;

delete from addresses;





alter table addresses add column city_name VARCHAR(50);
alter table addresses modify latitude VARCHAR(45);
alter table addresses modify alternate_name VARCHAR(150);
alter table addresses drop column address;
alter table addresses drop column address_id;
alter table addresses add column address_id INT NOT NULL primary key auto_increment;
alter table addresses add column address VARCHAR(100) NOT NULL;
alter table addresses add column county_name VARCHAR(45);
alter table addresses add column county_FIBS INT;



load data infile '/var/lib/mysql-files/addressesNew.csv'
into table Hospitals.addresses fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\n' (address,hospital_id,alternate_name,zip,zip4,latitude,longitude,city_name,county_name,county_FIBS,city_id);



select distinct c.city_id
from addresses as a join cities as c join cities on a.city_name=c.city_name;


select distinct a.county_FIBS, c.county_FIBS, a.county_name, c.county_name, a.city_name, c.city_name
from addresses a,cities c;

select c.city_id
from addresses a join cities c on a.county_name=c.county_name and a.county_FIBS=c.county_FIBS and a.city_name=c.city_name;

select distinct a.county_name
from addresses a join cities c on a.county_name=c.county_name;

alter table cities drop column county_name;
alter table cities drop column county_FIBS;


alter table addresses drop column county_name;
alter table addresses drop column county_FIBS;
alter table addresses drop column city_name;
alter table addresses drop column alternate_name;




#--------------------------------------------------------------------------------
select * from hospital_types;
select * from trauma_types;
select * from hospitals;
select * from countries;
select * from states;
select * from counties;
select * from cities;
select * from addresses;
select * from NAICS;
select * from sources;
select * from specifications;
select * from validation;


alter table NAICS modify NAICS_description longtext;

load data infile '/var/lib/mysql-files/NAICS.csv'
into table Hospitals.NAICS fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\r\n' (hospital_id,NAICS_code,NAICS_description);
select * from NAICS;

load data infile '/var/lib/mysql-files/specifications.csv'
into table Hospitals.specifications fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\r\n' (hospital_id,total_population,available_beds,helipad);
select * from specifications;

alter table sources modify source_name varchar(200);

load data infile '/var/lib/mysql-files/sources.csv'
into table Hospitals.sources fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\r\n' (hospital_id,source_name,source_date);
select * from sources;


load data infile '/var/lib/mysql-files/validations.csv'
into table Hospitals.validation fields terminated by ',' ENCLOSED BY '"' 
lines terminated by '\r\n' (hospital_id,validation_method,validation_date);
select * from validation;












