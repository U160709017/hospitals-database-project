#-----------------------------------------------------------------------------------------------------
select * from types;

load data infile '/var/lib/mysql-files/types.csv'
into table Hospitals.types fields terminated by ','
lines terminated by '\r' (types.hospital_type,types.trauma_type,types.trauma_level);

#alter table Hospitals.types drop types.type_id;
#delete from Hospitals.types;
alter table Hospitals.types auto_increment=1;
#alter table Hospitals.types add types.type_id int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;

#-----------------------------------------------------------------------------------------------------
select * from hospitals;

#ALTER TABLE hospitals MODIFY hospital_name varchar(100);
#ALTER TABLE hospitals MODIFY hospital_telephone varchar(20);
#ALTER TABLE hospitals MODIFY hospital_website varchar(300);

load data infile '/var/lib/mysql-files/hospitals.csv'
into table Hospitals.hospitals fields terminated by ',' enclosed by '"';

#-----------------------------------------------------------------------------------------------------
select * from hospitals_types;

load data infile '/var/lib/mysql-files/hospitals_types.csv'
into table Hospitals.hospitals_types fields terminated by ',' enclosed by '"'
lines terminated by '\r' (hospitals_types.hospital_id,hospitals_types.hospital_type,hospitals_types.trauma_type,hospitals_types.trauma_level);



insert into hospitals_types (type_id)
select t.type_id
from types t,hospitals_types h
where t.hospital_type=h.hospital_type and t.trauma_type=h.trauma_type and t.trauma_level=h.trauma_level;






