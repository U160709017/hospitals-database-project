alter table hospitals add hospital_type VARCHAR(45);

select t.hospital_type_id
from hospitals as h join hospital_types as t on h.hospital_type = t.hospital_type;

alter table hospitals drop column hospital_type;
#--------------------------------------------------------------------------------
delete from hospitals;
delete from trauma_types;

alter table hospitals drop column trauma_type;
alter table hospitals drop column trauma_level;

alter table hospitals add trauma_type_id INT;

load data infile '/var/lib/mysql-files/hospitalsNEW.csv'
into table Hospitals.hospitals fields terminated by ',' ENCLOSED BY '"'
LINES TERMINATED BY '\n' (hospital_id,hospital_name,hospital_telephone,hospital_status,hospital_owner,hospital_website,hospital_type_id,hospitals.trauma_type_id);

load data infile '/var/lib/mysql-files/trauma_types_NEW.csv'
into table Hospitals.trauma_types fields terminated by ','
lines terminated by '\n' (trauma_types.trauma_type, trauma_types.trauma_level);

select * from hospitals;
select * from trauma_types;

select t.trauma_type_id
from hospitals as h right join trauma_types as t on h.trauma_level=t.trauma_level and h.trauma_type=t.trauma_type;
#--------------------------------------------------------------------------------------------------------------------------

alter table states add column country_name VARCHAR(3);

load data infile '/var/lib/mysql-files/states.csv'
into table Hospitals.states fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\n' (state_name, state_FIBS, country_id);

delete from states;
select * from states;

select c.country_id
from countries as c join states as s on c.country_name = s.country_name;

alter table states drop column country_name;
#--------------------------------------------------------------------------------------------------------------------------
alter table counties add column state_name VARCHAR(2);

load data infile '/var/lib/mysql-files/counties.csv'
into table Hospitals.counties fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\n' (county_name, county_FIBS, state_id);

delete from counties;
select * from counties;

select s.state_id
from counties as c join states as s on c.state_name = s.state_name;

alter table counties drop column state_name;
#--------------------------------------------------------------------------------
alter table cities add column county_name VARCHAR(45);
alter table cities modify city_name varchar(50);

load data infile '/var/lib/mysql-files/cities.csv'
into table Hospitals.cities fields terminated by ',' ENCLOSED BY '"'
lines terminated by '\n' (city_name, county_name);

select * from cities;
delete from cities;

select distinct city_name, county_name
from cities;









