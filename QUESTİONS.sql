#1)What is the sum of county FIBS' in each county?
select county_name, sum(county_FIBS)
from counties
group by county_name;

#2)How many hospitals that have zip code 78503?
select count(*)
from hospitals h join addresses a on h.hospital_id = a.hospital_id
where a.zip = "78503";

#3)What is the average of total population in hospitals that have validation method is "IMAGERY"?
select avg(s.total_population)
from specifications s join (select h.hospital_id
from hospitals h join validation v on h.hospital_id=v.hospital_id
where v.validation_method="IMAGERY") as a on s.hospital_id=a.hospital_id;

#4)How many hospitals do have helipad?
select count(s.helipad)
from specifications s
where helipad = "1";

#5)Which children hospital does have maximum population?
select a.hospital_name
from hospital_types t join (select h.hospital_name,s.total_population,h.hospital_type_id
from hospitals h join specifications s on h.hospital_id=s.hospital_id
order by s.total_population desc) as a on t.hospital_type_id=a.hospital_type_id and hospital_type="CHILDREN"
limit 1;


#6)How many hospitals in county "Jack" have NAICS code 622110?
select count(*)
from hospitals join NAICS on hospitals.hospital_id = NAICS.hospital_id and NAICS_code = '622110' 
join addresses as a on a.hospital_id = hospitals.hospital_id join cities as c on c.city_id = a.city_id
join counties as ct on ct.county_id = c.county_id and ct.county_name = "JACK";

select count(*)
from NAICS n join (select a.hospital_id
from addresses a
where a.city_id=(select c.city_id
from cities c join counties ct on c.county_id=ct.county_id
where ct.county_name="Jack")) as x on n.hospital_id=x.hospital_id
where n.NAICS_code="622110";

#7)What is the minumum county FIBS in OH state?
select c.county_FIBS
from counties as c join cities as ct on c.county_id = ct.county_id join addresses as a on a.city_id = ct.city_id 
join states as s on s.state_id = c.state_id and state_name = "OH"
order by c.county_FIBS asc
limit 1;

#8)What is the average of available beds in hospitals that have status open?
select avg(available_beds)
from hospitals join specifications on hospitals.hospital_id = specifications.hospital_id
where hospital_status = "1";

#9)How many hospitals have helipad in the city Winslow?
select count(*)
from hospitals as h join addresses as a on h.hospital_id=a.hospital_id join cities as c on c.city_id = a.city_id and c.city_name ="WINSLOW";


#10)What is the minumum latitude in PRI?
select min(latitude)
from addresses as a join cities as c on a.city_id = c.city_id join counties as ct on ct.county_id = c.county_id 
join states as s on s.state_id = ct.state_id join countries as cnt on s.country_id=cnt.country_id and cnt.country_name = "PRI";


#11)How many hospitals do each type have?
select hospital_type, count(hospital_id)
from hospitals join hospital_types on hospitals.hospital_type_id = hospital_types.hospital_type_id
group by hospital_type;
