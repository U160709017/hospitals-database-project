#----------------OUT STORED PROCEDURE------------------------#

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CountValidationMethodByDate`(
 IN ValDate DATE,
 OUT total INT)
BEGIN
 SELECT count(validation_method)
 INTO total
 FROM validation
 WHERE validation_date = ValDate;
END$$
DELIMITER ;
CALL CountValidationMethodByDate('2018-04-20',@total);
select @total;

#----------------IN STORED PROCEDURE------------------------#
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `SelectHospitalsByCity`
(IN CityName VARCHAR(45))
BEGIN
 SELECT hospital_name
 from hospitals as h join addresses as a on h.hospital_id = a.hospital_id 
 join cities as c on  c.city_id = a.city_id and city_name = CityName;
 END$$
DELIMITER ;
call SelectHospitalsByCity('HOUSTON');
#----------------INOUT STORED PROCEDURE------------------------#
DELIMITER $$
CREATE PROCEDURE hospitalStatus (INOUT hStatus INT, IN hosp_status binary(1))
BEGIN
SELECT COUNT(hospitals.hospital_status) INTO hStatus FROM hospitals WHERE hospitals.hospital_status = hosp_status;
END$$
DELIMITER ;

CALL hospitalStatus(@C,'1');
select @C;

#----------------VIEW------------------------#

CREATE VIEW `Hospitals in HOUSTON` AS 
select `h`.`hospital_name` AS `hospital_name` 
from ((`hospitals` `h` join `addresses` `a` on((`h`.`hospital_id` = `a`.`hospital_id`))) 
join `cities` `c` on(((`c`.`city_id` = `a`.`city_id`) and (`c`.`city_name` = 'HOUSTON'))));

select * from `Hospitals in HOUSTON`;


